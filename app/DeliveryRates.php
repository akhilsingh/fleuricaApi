<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryRates extends Model
{
   public $table = 't_delivery_rates';
}
