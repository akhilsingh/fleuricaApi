<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => '/api/v1'], function() {

        Route::resource('/merchant', 'Rest\MerchantController');
		Route::resource('/customer', 'Rest\CustomerController');
		Route::resource('/products', 'Rest\ProductsController');
		Route::resource('/cart',     'Rest\CartController');
		Route::resource('/order',    'Rest\OrderController');
		Route::resource('/attribute','Rest\AttributeController');
		Route::resource('/coupon',   'Rest\CouponController');
		Route::resource('/deliveryRates', 'Rest\DeliveryRatesController');
		Route::resource('/surcharges', 'Rest\SurchargesController');
		Route::resource('/messages', 'Rest\MessagesController');

	});
